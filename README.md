ThinkPHP 6.0
===============

> 运行环境要求PHP7.1+。

[官方应用服务市场](https://market.topthink.com) | [`ThinkPHP`开发者扶持计划](https://sites.thinkphp.cn/1782366)

ThinkPHPV6.0版本由[亿速云](https://www.yisu.com/)独家赞助发布。

## 主要新特性

* 采用`PHP7`强类型（严格模式）
* 支持更多的`PSR`规范
* 原生多应用支持
* 更强大和易用的查询
* 全新的事件系统
* 模型事件和数据库事件统一纳入事件系统
* 模板引擎分离出核心
* 内部功能中间件化
* SESSION/Cookie机制改进
* 对Swoole以及协程支持改进
* 对IDE更加友好
* 统一和精简大量用法
* 基于`Auth`验证的权限管理系统
    * 支持无限级父子级权限继承，父级的管理员可任意增删改子级管理员及权限设置
    * 支持管理子级数据或个人数据
* 强大的一键生成功能
    * 一键生成CRUD,包括控制器、模型、视图、菜单、等
    * 一键生成控制器菜单和规则
* 完善的前端功能组件开发
* 基于`layui`开发，自适应手机、平板、PC
* 适应PC及手机端
* 待开发.....


## 安装

~~~
下载安装包 http://你的域名/install
~~~

如果需要更新框架使用
~~~
composer update topthink/framework
~~~
## **在线演示**
https://novice.qiersan.com
用户名：abc123

密　码：123456
强大一键生成菜单，控制器，模型，视图，增 删 改 查功能，如需重写直接找到对应的控制器编写自己的功能。
![输入图片说明](https://images.gitee.com/uploads/images/2020/1113/100248_1c7f11b8_2180001.png "屏幕截图.png")
基于Auth权限验证 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1113/100417_575ab427_2180001.png "屏幕截图.png")

提　示：演示站数据无法进行修改，请下载源码安装体验全部功能

QQ群: [550262220](https://jq.qq.com/?_wv=1027&k=haOXCLJq)
## 文档

[完全开发手册](https://www.kancloud.cn/manual/thinkphp6_0/content)

## 参与开发

请参阅 [ThinkPHP 核心框架包](https://github.com/top-think/framework)。
## **特别鸣谢**

感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

Layui框架。


## 版权信息

ThinkPHP遵循Apache2开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2006-2020 by ThinkPHP (http://thinkphp.cn)

All rights reserved。

ThinkPHP® 商标和著作权所有者为上海顶想信息科技有限公司。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
