/*
 NoviceAdmin Install SQL

 官网: https://novice.qiersan.com
 演示: https://novice.qiersan.com
 @author 雷雷
 Date: 2018年05月26日
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for na_admin
-- ----------------------------
DROP TABLE IF EXISTS `na_admin`;
CREATE TABLE `na_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `phone` varchar(12) NOT NULL DEFAULT '' COMMENT '手机号码',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录IP',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Session标识',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `delete_time` int(10) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of na_admin
-- ----------------------------
BEGIN;
INSERT INTO `na_admin` VALUES (1, 'admin', 'Admin', '075eaec83636846f51c152f29b98a2fd', 's4f3', 13888888888,'', 'admin@noviceadmin.net', 0, 1605080365, '127.0.0.1',1605080365, 1605080365, 'd3992c3b-5ecc-4ecb-9dc2-8997780fcadc', 1,null);
COMMIT;

-- ----------------------------
-- Table structure for na_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `na_admin_log`;
CREATE TABLE `na_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `name` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员日志表';

-- ----------------------------
-- Table structure for na_attachment
-- ----------------------------
DROP TABLE IF EXISTS `na_attachment`;
CREATE TABLE `na_attachment` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) NOT NULL DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) DEFAULT NULL COMMENT '创建日期',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `uploadtime` int(10) DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='附件表';

-- ----------------------------
-- Records of na_attachment
-- ----------------------------
BEGIN;
INSERT INTO `na_attachment` VALUES (1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 21859, 'image/png', '', 1605080365, 1605080365, 1605080365, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6');
COMMIT;

-- ----------------------------
-- Table structure for na_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `na_auth_group`;
CREATE TABLE `na_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '规则ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `delete_time` int(10) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='分组表';


-- ----------------------------
-- Records of na_auth_group
-- ----------------------------
BEGIN;
INSERT INTO `na_auth_group` VALUES (1, 0, '超级管理员', '*', 1605080365, 1605080365, 1,null);

COMMIT;

-- ----------------------------
-- Table structure for na_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `na_auth_group_access`;
CREATE TABLE `na_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '级别ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限分组表';

-- ----------------------------
-- Records of na_auth_group_access
-- ----------------------------
BEGIN;
INSERT INTO `na_auth_group_access` VALUES (1, 1);
COMMIT;

-- ----------------------------
-- Table structure for na_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `na_auth_rule`;
CREATE TABLE `na_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态:(0:隐藏,1:显示)',
  `delete_time` int(10) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='节点表';

-- ----------------------------
-- Records of na_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `na_auth_rule` VALUES (1, 'menu', 18, 'sys', '系统设置', 'fa-cog', '', '', 1, 1605080365, 1605080365, 143, 1,null);
INSERT INTO `na_auth_rule` VALUES (2, 'menu', 3, 'database/index', '在线命令', 'fa-cogs', '', '', 1, 1605080365, 1605080365, 137, 1,null);
INSERT INTO `na_auth_rule` VALUES (3, 'menu', 18, 'admin', '菜单管理', 'fa-th', '', '', 1, 1605080365, 1605080365, 119, 1,null);
INSERT INTO `na_auth_rule` VALUES (4, 'menu', 3, 'admin/index', '管理员列表', 'fa-rocket', '', '', 1, 1605080365, 1605080365, 0, 1,null);
INSERT INTO `na_auth_rule` VALUES (5, 'file', 4, 'admin/add', '添加管理员', 'fa-group', '', '', 1, 1605080365, 1605080365, 99, 1,null);
INSERT INTO `na_auth_rule` VALUES (6, 'file', 4, 'admin/edit', '编辑管理员', 'fa fa-cog', '', '', 1, 1605080365, 1605080365, 60, 1,null);
INSERT INTO `na_auth_rule` VALUES (7, 'file', 4, 'admin/delete', '管理员删除', 'fa fa-file-image-o', '', '', 1, 1605080365, 1605080365, 53, 1,null);
INSERT INTO `na_auth_rule` VALUES (8, 'file', 4, 'admin/addData', '管理员数据提交', 'fa fa-user', '', '', 1, 1605080365, 1605080365, 34, 1,null);
INSERT INTO `na_auth_rule` VALUES (9, 'file', 4, 'admin/editData', '管理员数据修改', 'fa fa-user', '', '', 1, 1605080365, 1605080365, 118, 1,null);
INSERT INTO `na_auth_rule` VALUES (10, 'menu', 3, 'authRule/index', '权限列表', 'fa fa-list-alt', '', '', 1, 1605080365, 1605080365, 113, 1,null);
INSERT INTO `na_auth_rule` VALUES (11, 'file', 10, 'authRule/add', '权限增加', 'fa fa-group', '', '', 1, 1605080365, 1605080365, 109, 1,null);
INSERT INTO `na_auth_rule` VALUES (12, 'file', 10, 'authRule/edit', '权限修改', 'fa fa-bars', '', '', 1, 1605080365, 1605080365, 104, 1,null);
INSERT INTO `na_auth_rule` VALUES (13, 'file', 10, 'authRule/delete', '权限删除', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 136, 1,null);
INSERT INTO `na_auth_rule` VALUES (14, 'menu', 3, 'authGroup/index', '角色列表', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 135, 1,null);
INSERT INTO `na_auth_rule` VALUES (15, 'file', 14, 'authGroup/add', '角色添加', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 133, 1,null);
INSERT INTO `na_auth_rule` VALUES (16, 'file', 14, 'authGroup/edit', '角色修改', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 134, 1,null);
INSERT INTO `na_auth_rule` VALUES (17, 'file', 14, 'authGroup/delete', '角色删除', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 132, 1,null);
INSERT INTO `na_auth_rule` VALUES (18, 'menu', 0, 'A', '常规管理', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 52, 1,null);
INSERT INTO `na_auth_rule` VALUES (19, 'menu', 1, 'sys/setting', '基础设置', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 51, 1,null);
INSERT INTO `na_auth_rule` VALUES (20, 'menu', 1, 'sys/oss', '存储设置', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 50, 1,null);
INSERT INTO `na_auth_rule` VALUES (21, 'menu', 1, 'sys/weixin', '微信设置', 'fa fa-circle-o', '', '', 0, 1605080365, 1605080365, 49, 1,null);
COMMIT;




-- ----------------------------
-- Table structure for na_config
-- ----------------------------
DROP TABLE IF EXISTS `na_config`;
CREATE TABLE `na_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text NOT NULL COMMENT '变量值',
  `content` text NOT NULL COMMENT '变量字典数据',
  `rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) NOT NULL DEFAULT '' COMMENT '扩展属性',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统配置';

-- ----------------------------
-- Records of na_config
-- ----------------------------
BEGIN;
INSERT INTO `na_config` VALUES (1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', 'qiersan', '', 'required', '');
INSERT INTO `na_config` VALUES (2, 'beian', 'basic', 'Beian', '滇ICP备2020008464号', 'string', '', '', '', '');

COMMIT;




-- ----------------------------
-- Table structure for na_user
-- ----------------------------
DROP TABLE IF EXISTS `na_user`;
CREATE TABLE `na_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '组别ID',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date COMMENT '生日',
  `bio` varchar(100) NOT NULL DEFAULT '' COMMENT '格言',
  `money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `successions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `prevtime` int(10) DEFAULT NULL COMMENT '上次登录时间',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) NOT NULL DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `joinip` varchar(50) NOT NULL DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) DEFAULT NULL COMMENT '加入时间',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'Token',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  `verification` varchar(255) NOT NULL DEFAULT '' COMMENT '验证',
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `email` (`email`),
  KEY `mobile` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='会员表';

-- ----------------------------
-- Records of na_user
-- ----------------------------
BEGIN;
INSERT INTO `na_user` VALUES (1, 1, 'admin', 'admin', 'c13f62012fd6a8fdf06b3452a94430e5', 'rpR6Bv', 'admin@163.com', '18888888888', '', 0, 0, '2020-11-11', '', 0, 0, 1, 1, 1516170492, 1516171614, '127.0.0.1', 0, '127.0.0.1', 1491461418, 0, 1516171614, '', 'normal','');
COMMIT;


