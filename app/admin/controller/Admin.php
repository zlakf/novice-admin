<?php
/**
*@author 雷雷
*/
namespace app\admin\controller;

use app\admin\model\AuthGroup as AuthGroupModel;
use think\facade\View;
use think\facade\Db;
use think\facade\Session;
use app\admin\model\Admin as AdminModel;
class Admin extends Base
{
    public function index()
    {
        $AdminModel = new AdminModel();
        $data =request()->param('searchParams');
        if (request()->param('limit') != null && request()->param('page') != null){
            $search = json_decode($data, true);
            if($search==Null){
                $search = ['id'=>0];
            }
            $list =$AdminModel->withSearch(array_keys($search),$search)->field(['id','username','nickname','email'])->paginate([
                'list_rows'=> request()->param('limit'),
                'var_page' => 'page',
                'query' =>request()->param()
            ]);

            $count = $AdminModel->count();
            return $this->success($list,'成功',$count,0);
        }
        return View::fetch();
    }

    /**添加页
     * @return string
     */
    public function add(){

        $AuthGroupModel = new AuthGroupModel();
         View::assign('group',$AuthGroupModel ->select());
        return View::fetch();
    }
    /**
     * 编辑页
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(){
        $id = request()->param('id');
        $list = Db::name('admin')
            ->alias('a')
            ->join('auth_group_access aga','a.id = aga.uid')
            ->join('auth_group ag','aga.group_id = ag.id')
            ->where('a.id',$id)
            ->field(['a.*','ag.title','aga.group_id'])
            ->find();
        $AuthGroupModel = new AuthGroupModel();
        View::assign('group',$AuthGroupModel ->where('status',1)->select());
        View::assign('data',$list);
        return View::fetch();
    }
    /**添加管理员
     * @return \think\Response|\think\response\Redirect
     */
    public function addData()
    {
        $data = request()->param();
        $AdminModel = new AdminModel();
        $salt=$this->salt();
        $data['password'] =sha1($data['password'].$salt);
        $data['salt'] = $salt;

        // 启动事务
        Db::startTrans();
        try {
            $uid = $AdminModel->insertGetId($data);
            $info = ['uid' => $uid, 'group_id' => $data['group_id']];
            Db::name('auth_group_access')->insert($info);
            // 提交事务
            Db::commit();
            return $this->success([],'成功~',0,200);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->success([],'失败~',0,400);

        }


    }
    /**
     * 编辑提交
     * @return \think\Response|\think\response\Redirect
     */
    public function editData(){

        $data = request()->param();
        if (empty($data['password'])){
            unset($data['password']);
        }else{
            $salt=$this->salt();
            $data['password'] =sha1($data['password'].$salt);
            $data['salt'] = $salt;
        }
        // 启动事务
        Db::startTrans();
        try {
            Db::name('auth_group_access')->where('uid',$data['id'])->save([
                'group_id' => $data['group_id']
            ]);
            AdminModel::update($data);
            // 提交事务
            Db::commit();
            return $this->success([],'成功~',0,200);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return $this->success([],'失败~',0,400);

        }

    }

    /**
     * 删除
     * @return \think\Response
     */
    public function delete()
    {
        $id = request()->param('id');
        $AdminModel = new AdminModel();
        $res = $AdminModel->where('id',$id)->delete();
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,400);
        }

    }

    /**批量删除
     * @return string
     */
    public function batchDelete(){
        $ids = request()->param('data');
        $data = json_decode($ids);
        $arr = $this->object_array($data);
        foreach ($arr as $k =>$v) {
            // 软删除
            $res = AdminModel::destroy($v['id']);

        }
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,4000);
        }
    }

}