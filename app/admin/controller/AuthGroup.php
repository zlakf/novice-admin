<?php


namespace app\admin\controller;
use app\admin\model\AuthGroup as AuthGroupModel;
use app\admin\model\AuthRule as AuthRuleModel;
use think\facade\View;
use app\admin\middleware\Auth as AuthMiddleware;

class AuthGroup extends Base
{
    protected $middleware = [AuthMiddleware::class];

    /**
     *
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $AuthGroupModel = new AuthGroupModel();
        $data =request()->param('searchParams');
        if (request()->param('limit') != null && request()->param('page') != null){
            $search = json_decode($data, true);
            if($search==Null){
                $search = ['id'=>0];
            }
            $list =$AuthGroupModel->withSearch(array_keys($search),$search)->paginate([
                'list_rows'=> request()->param('limit'),
                'var_page' => 'page',
                'query' =>request()->param()
            ]);

            $count = $AuthGroupModel->count();
            return $this->success($list,'成功',$count,0);
        }
        return View::fetch();

    }

    /**添加页
     * @return string
     */
    public function add(){


        return View::fetch();
    }
    /**
     * 编辑页
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(){
        $id = request()->param('id');
        $AuthGroupModel = new AuthGroupModel();
        $data = $AuthGroupModel->where('id',$id)->find();
        //$data['rules'] = explode(',',$data['rules']);
        $arr = explode(",", $data['rules']);
        $AuthRuleModel = new AuthRuleModel();
        $rules = $AuthRuleModel->where('type','file')
            ->where('status',1)
            ->column('id');
        $pid = $AuthRuleModel->where('type','menu')
            ->where('status',1)
            ->column('pid');
        $menu =$AuthRuleModel->where('type','menu')
            ->where('status',1)
            ->where('id','not in',$pid)
            ->column('id');

        $ret= array_merge($rules, $menu);
        $result = array_intersect($arr, $ret);
        $str = implode(',',$result);
        $data['rules'] = $str;
        View::assign('data',$data);
        return View::fetch();
    }

    /**添加角色
     * @return \think\Response|\think\response\Redirect
     */
    public function addData()
    {
        $data = request()->param();
        $AuthGroupModel = new AuthGroupModel();
        $res = $AuthGroupModel->save($data);
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,400);
        }

    }
    /**
     * 编辑提交
     * @return \think\Response|\think\response\Redirect
     */
    public function editData(){
     
        $data = request()->param();
        $res = AuthGroupModel::update($data);
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,4000);
        }
    }

    /**
     * 删除
     * @return \think\Response
     */
    public function delete()
    {
        $id = request()->param('id');
        $AuthGroupModel = new AuthGroupModel();
        $res = $AuthGroupModel->where('id',$id)->delete();
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,400);
        }

    }
    /**批量删除
     * @return string
     */
    public function batchDelete(){
        $ids = request()->param('data');
        $data = json_decode($ids);
        $arr = $this->object_array($data);
        foreach ($arr as $k =>$v) {
            // 软删除
            $res = AuthGroupModel::destroy($v['id']);

        }
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,4000);
        }
    }
}
