<?php


namespace app\admin\controller;
use app\admin\middleware\Auth as AuthMiddleware;
use app\admin\model\AuthRule as AuthRuleModel;
use think\db\Where;
use think\facade\Db;
use think\facade\Request;
use think\facade\View;


class AuthRule extends Base
{
    protected $middleware = [AuthMiddleware::class];
    /**
     * 权限列表页
     * @return string
     */
    public function index(){

            $AuthRuleModel = new AuthRuleModel();
            $data =request()->param('searchParams');
            if (request()->param('limit') != null && request()->param('page') != null){
                $search = json_decode($data, true);
                if($search==Null){
                    $search = ['id'=>0];
                }
                $list =$AuthRuleModel->withSearch(array_keys($search),$search)->paginate([
                    'list_rows'=> request()->param('limit'),
                    'var_page' => 'page',
                    'query' =>request()->param()
                ]);

                $count = $AuthRuleModel->count();
                return $this->success($list,'成功',$count,0);
            }

            //重定向
            return View::fetch();

    }

    /**添加页
     * @return string
     */
    public function add(){


        return View::fetch();
    }

    /**
     * 编辑页
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(){
        $id = request()->param('id');
        $AuthRuleModel = new AuthRuleModel();
        $data = $AuthRuleModel->where('id',$id)->find();
        $data['ptitle'] =  $AuthRuleModel->where('id',$data['pid'])->find()['title'];
        View::assign('data',$data);
        return View::fetch();
    }

    /**
     * 树形节点
     * @return \think\Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function treeRule(){
        $AuthRuleModel = new AuthRuleModel();
        $list =$AuthRuleModel->field(["id","pid","title","name"])->select();
        $data = $this->getTree($list,0);
//        $arr =['id'=>0,'pid'=>0,'name'=>'Admin','title'=>'VR系统','children'=>[]];
//        array_unshift($data,$arr);
        foreach ($data as $k =>$v){
            $data[$k]['title'] = $v['title'].'【'.$v['name'].'】';
        }
        return $this->success($data,"成功");
    }
    /**
     * 树形节点
     * @return \think\Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function ruleAdd(){
        $AuthRuleModel = new AuthRuleModel();
        $list =$AuthRuleModel->field(["id","pid","title","name"])->where('type','menu')->select();
        $data = $this->getTree($list,0);
//        $arr =['id'=>0,'pid'=>0,'name'=>'Admin','title'=>'VR系统','children'=>[]];
//        array_unshift($data,$arr);
        foreach ($data as $k =>$v){
            $data[$k]['title'] = $v['title'].'【'.$v['name'].'】';
        }
        return $this->success($data,"成功");
    }

    /**添加权限
     * @return \think\Response|\think\response\Redirect
     */
    public function addData()
    {
        //仅能POST访问
        if(!(request()->isPost())){
            //重定向
            return redirect((string)url('/index'));
        }

        $data = request()->param();
        $AuthRuleModel = new AuthRuleModel();
        $result =$AuthRuleModel->where('name',$data['name'])->find();
        $res =$AuthRuleModel->where('title',$data['title'])->find();
        if (!empty($res)){
            return $this->success([],'标题已存在~',0,400);
        }
        if (!empty($result)){
            return $this->success([],'规则已存在~',0,400);
        }

        $res = $AuthRuleModel->save($data);
        if ($res == 1){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,400);
        }

    }

    /**
     * 编辑提交
     * @return \think\Response|\think\response\Redirect
     */
    public function editData(){
        //仅能POST访问
        if(!(request()->isPost())){
            //重定向
            return redirect((string)url('/index'));
        }
        $data = request()->param();
        $res = AuthRuleModel::update($data);
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,4000);
        }
    }
    public function delete()
    {
        $id = request()->param('id');
        $AuthRuleModel = new AuthRuleModel();
        $data = $AuthRuleModel->where('pid',$id)->select();
        if ($data){
            foreach ($data as $k => $v){
                $AuthRuleModel->where('id',$v['id'])->delete();
            }
        }
        $res = $AuthRuleModel->where('id',$id)->delete();
        if ($res){
            return $this->success([],'成功~',0,200);
        }else{
            return $this->success([],'失败~',0,400);
        }

    }
    /**批量删除
     * @return string
     */
    public function batchDelete(){
        $ids = request()->param('data');
        $obj = json_decode($ids);
        $arr = $this->object_array($obj);

        foreach ($arr as $k =>$v) {
            try {
                $data = AuthRuleModel::where('pid',$v['id'])->select();
                if ($data){
                    foreach ($data as $kk => $vv){
                        // 软删除
                        AuthRuleModel::destroy($vv['id']);
                    }
                }
                AuthRuleModel::destroy($v['id']);
                return $this->success([],'成功~',0,200);

            }catch (\Exception $e){
                return $this->success([],'失败~',0,400);
            }



        }

    }
}