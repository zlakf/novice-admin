<?php

namespace app\admin\controller;


use app\BaseController;
use think\exception\HttpResponseException;
use think\facade\Request;
use think\facade\View;
use think\Response;
use think\wenhainan\Auth;
use app\admin\model\AuthRule as AuthRuleModel;
class Base extends BaseController
{
    protected $page;
    protected $pageSize;
    public $adminUser=null;
    public function initialize(){
        //继承父类初始化
        parent::initialize();
        //后置中间件
        $auth = new Auth;
        $url = Request::controller();
        $grouplist = $auth ->getGroups(session('admin'));
        if (!empty(session('admin'))){
            if ($grouplist[0]['rules'] != '*'){
                if(!$auth->check($url.'/'.'add', session('admin'))){
                    View::assign('add','false');
                }else{
                    View::assign('add','true');
                }
                if(!$auth->check($url.'/'.'edit', session('admin'))){
                    View::assign('edit','false');
                }else{
                    View::assign('edit','true');
                }
                if(!$auth->check($url.'/'.'delete', session('admin'))){
                    View::assign('delete','false');
                }else{
                    View::assign('delete','true');
                }
            }else{
                View::assign('add','true');
                View::assign('edit','true');
                View::assign('delete','true');
            }
        }


       
    }

    protected function success($data, string $msg = '', $count = 0,int $code=200,string $type='json'):Response
    {
        //标准API生成结构
        $result=[
            //状态码
            'code' => $code,
            //消息
            'msg'  => $msg,
            //数量
            'count' => $count,
            //数据
            'data' => $data
        ];
        //返回API接口
        return Response::create($result,$type);
    }

    //public function __call($name, $arguments)
    //{
        //404访问不存在
      //  return $this->create([],'资源不存在~',404);
    //}
    /**密码盐
     * @author zhoulei
     * @return false|string
     */
    protected function salt()
    {
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $salt = substr(str_shuffle($str),10,5);
        return $salt;
    }

    /**
     * 递归无限极
     * @param $list
     * @param $pid
     * @return array
     */
    public function getTree($list,$pid)
    {
        $tree = array();
        foreach($list as $k => $v)
        {
            if($v['pid'] == $pid)
            {         //父亲找到儿子
                $v['children'] = $this->getTree($list, $v['id']);
                $tree[] = $v;
            }
        }
        return $tree;
    }

    /**
     * PHP stdClass Object转array
     * @param $array
     * @return array
     */
    function object_array($array) {
        if(is_object($array)) {
            $array = (array)$array;
        } if(is_array($array)) {
            foreach($array as $key=>$value) {

                $array[$key] = $this->object_array($value);
            }
        }
        return $array;
    }
}