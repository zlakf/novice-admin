<?php
namespace app\admin\controller;

use think\facade\View;
use think\facade\Db;
use think\facade\Session;

class Database
{
    public function index()
    {
       $tables = Db::query("show tables");
       $arr ='';
       foreach ($tables as $key => $value) {
       	foreach ($value as $k => $v) {
       		# code...
       		$arr.=$v.',';
       	}
       	
       }
       $newstr = substr($arr,0,strlen($arr)-1); 
       $pieces = explode(",", $newstr);
      	View::assign('tables',$pieces);
        return View::fetch();
    }
}