<?php
declare (strict_types = 1);

namespace app\admin\controller;
use app\admin\model\Admin as AdminModel;
use app\admin\model\AuthRule as AuthRuleModel;
use app\validate\admin\AdminUser as AdminUserValidate;
use app\BaseController;
use think\exception\ValidateException;
use think\facade\View;
use think\facade\Db;
use think\facade\Session;
use think\helper\Arr;
use think\wenhainan\Auth;


class Index extends Base
{
    /**后台首页
     * @return string
     */
    public function index()
    {
        $uid = \session('admin');
        $AdminModel = new AdminModel();
        $nickname = $AdminModel->where('id',$uid)->find()['nickname'];
        View::assign('nickname',$nickname);
        return View::fetch();
    }
    public function add()
    {
        $data=[
            'username' => 'admin',
            'password' => '123456',
        ];
        $salt=$this->salt();
        $data['password'] =sha1($data['password'].$salt);
        $data['salt'] = $salt;
        $AdminModel=new AdminModel();
        $AdminModel->add($data);

    }


  
    // 获取初始化数据
    public function getSystemInit(){
        $homeInfo = [
            'title' => '控制台',
            'href'  => 'admin/sys/index.html',
        ];
        $logoInfo = [
            'title' => 'NoviceAdmin',
            'image' => 'static/images/logo.png',
        ];
        $menuInfo =$this->getMenuList();
        $systemInit = [
            'homeInfo' => $homeInfo,
            'logoInfo' => $logoInfo,
            'menuInfo' => $menuInfo,
        ];
        return json($systemInit);
    }

    // 获取菜单列表
    private function getMenuList(){

        $AuthRuleModel = new AuthRuleModel();
        $auth = new Auth();
        $grouplist = $auth->getGroups(session('admin'));
        if ($grouplist[0]['rules']!= '*'){
            $menuList =$AuthRuleModel->field(["id","pid","title","name","icon"])->where('type','menu')->where('id','in',$grouplist[0]['rules'])->order('weigh','desc')->select();
        }else{
            $menuList =$AuthRuleModel->field(["id","pid","title","name","icon"])->where('type','menu')->order('weigh','desc')->select();
        }

        foreach ($menuList as $k =>$v){
            if($v['name']!=''){
            	$menuList[$k]['href'] ='admin/'.$v['name'];
            }else{
            	$menuList[$k]['href'] = $v['name'];
            }
            $menuList[$k]['target'] ='_self';
            $menuList[$k]['icon'] ='fa '.$v['icon'];
        }
        $menuList = $this->buildMenuChild(0, $menuList);
        return $menuList;
    }

    //递归获取子菜单
    private function buildMenuChild($pid, $menuList){
        $treeList = [];
        foreach ($menuList as $v) {
            if ($pid == $v['pid']) {
                $node = $v;
                $child = $this->buildMenuChild($v['id'], $menuList);
                if (!empty($child)) {
                    $node['child'] = $child;
                }
                //
                $treeList[] = $node;
            }
        }
        return $treeList;
    }
    public function user_setting()
    {
        $info =AdminModel::where('id',session('admin'))->field(['username','phone','email'])->find();
        if((request()->isPost())){
            $user = AdminModel::find(session('admin'));
            $user->username     = request()->param('username');
            $user->phone     = request()->param('phone');
            $user->email    = request()->param('email');
            $res =$user->force()->save();
            if ($res){
                return $this->success([],'成功~',0,200);
            }else{
                return $this->success([],'失败~',0,4000);
            }
        }
        View::assign('info',$info);
        return View::fetch();
    }
    public function user_password()
    {
        $info =AdminModel::where('id',session('admin'))->find();
        if((request()->isPost())){
            $data = request()->param();
            $password = sha1($data['old_password'].$info['salt']);
            if ($password != $info['password']){
                return $this->success([],'旧密码错误',0,400);
            }
            if($data['new_password'] != $data['again_password']){
                return $this->success([],'两次输入密码不一致',0,400);
            }
            $salt=$this->salt();
            $password =sha1($data['new_password'].$salt);
            $user = AdminModel::find(session('admin'));
            $user->password     = $password;
            $user->salt    = $salt;
            $res = $user->save();
            if ($res){
                return $this->success([],'成功~',0,200);
            }else{
                return $this->success([],'失败~',0,4000);
            }
        }
        return View::fetch();
    }

}
