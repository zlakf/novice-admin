<?php
declare (strict_types = 1);
namespace app\admin\controller;

use app\admin\middleware\Auth as AuthMiddleware;
use think\facade\View;
use think\facade\Db;
use think\facade\Session;

class Page
{
    protected $middleware = [AuthMiddleware::class];
    public function page()
    {
        return View::fetch('welcome_one');
    }

    public function menu()
    {
        return View::fetch();
    }

    public function setting()
    {
        return View::fetch();
    }

    public function table()
    {
        return View::fetch();
    }
    public function form()
    {
        return View::fetch();
    }
    public function layer()
    {
        return View::fetch();
    }
    public function button()
    {
        return View::fetch();
    }

    public function welcome_one()
    {
        return View::fetch();
    }
    public function welcome_two()
    {
        return View::fetch();
    }
    public function welcome_three()
    {
        return View::fetch();
    }

    public function login_one()
    {
        return View::fetch();
    }
    public function login_two()
    {
        return View::fetch();
    }
    public function login_three()
    {
        return View::fetch();
    }

}
