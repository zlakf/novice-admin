<?php
declare (strict_types = 1);

namespace app\admin\middleware;
use app\admin\controller\Base;
use think\wenhainan\Auth as AuthWenhainan;
class Auth extends Base
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {

        $auth = new AuthWenhainan();
        //得到当前url
        $url = $request->controller().'/'.$request->action();
        $grouplist = $auth ->getGroups(session('admin'));
        if ($grouplist[0]['rules'] != '*'){
            if(!$auth->check($url, session('admin'))){

               halt("你没有操作权限");
            }
        }

        return $next($request);
    }

}
