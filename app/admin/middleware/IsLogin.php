<?php


namespace app\admin\middleware;


use app\admin\model\AuthRule as AuthRuleModel;
use think\facade\View;
use think\Response;
use think\wenhainan\Auth;

class IsLogin
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {

        $auth = new Auth();
        $AuthRuleModel = new AuthRuleModel();
        //前置中间件
        if(empty(session('admin')) && !preg_match('/login/',$request->pathinfo())){
            return redirect(url('login/index'));
        }
        if (!empty(session('admin'))){
            $rules = $auth->getGroups(session('admin'));
            $arr = explode(",",$rules[0]['rules']);
            $list = [];
            foreach ($arr as $k => $v){
                $list[]= $AuthRuleModel->where('id',$v)->find()['name'];
            }
            View::assign('btns',$list);
        }

        return $next($request);
    }


}