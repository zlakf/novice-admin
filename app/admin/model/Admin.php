<?php


namespace app\admin\model;


use think\Collection;
use think\Model;
use think\model\concern\SoftDelete;

class Admin extends Model
{
    use SoftDelete;
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';


    public function searchIdAttr($query,$value,$data){
        $query->where('id','>',$value);
    }
    /**搜索器
     * @param $query
     * @param $value
     * @param $data
     */
    public function searchUserNameAttr($query,$value,$data){
        $query->where('username','like','%'.$value.'%');
    }
    /**搜索器
     * @param $query
     * @param $value
     * @param $data
     */
    public function searchNickNameAttr($query,$value,$data){
        $query->where('nickname','like','%'.$value.'%');
    }
    /**搜索器
     * @param $query
     * @param $value
     * @param $data
     */
    public function searchEmailAttr($query,$value,$data){
        $query->where('email','like','%'.$value.'%');
    }
    /**
     * 添加管理员
     */
    public function add($data){
        $this->save($data);
    }

    /**
     * 查找管理员
     * @param $data
     */
    public function findUser($data)
    {
        return $this->where('username',$data)->find();
    }

    /**
     * 管理员登录
     * @param $data
     * @param $key
     */
    public function updateLoginTime($data,$key){
        return $this->update($data,['username'=>$key]);
    }

}