<?php


namespace app\admin\model;


use think\Model;
use think\model\concern\SoftDelete;

class AuthGroup extends Model
{
    use SoftDelete;
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';

    public function searchIdAttr($query,$value,$data){
        $query->where('id','>',$value);
    }
    /**搜索器
     * @param $query
     * @param $value
     * @param $data
     */
    public function searchTitleAttr($query,$value,$data){
        $query->where('title','like','%'.$value.'%');
    }

}