<?php
        namespace app\admin\model;
        use think\Collection;
        use think\Model;
        
        class User extends Model
        {
            // 开启自动写入时间戳字段
            protected $autoWriteTimestamp = 'int';
            // 定义时间戳字段名
            protected $createTime = 'add_time';
            protected $updateTime = 'update_time';
        
        
            public function searchIdAttr($query,$value,$data){
                $query->where('id','>',$value);
            }
           
           }