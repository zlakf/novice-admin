<?php


namespace app\index\controller;


use app\Request;
use think\Response;

abstract class Base
{
    protected $page;
    protected $pageSize;
    public function __construct()
    {
        //获取分页
        $this->page = \think\facade\Request::param('page');
        //获取条数
        $this->pageSize = \think\facade\Request::param('page_size',10);
    }
    protected function create($data, string $msg = '', int $code=200,string $type='json'):Response
    {
        //标准API生成结构
        $result=[
            //状态码
            'code' => $code,
            //消息
            'msg'  => $msg,
            //数据
            'data' => $data
        ];
        //返回API接口
        return Response::create($result,$type);
    }
    public function __call($name, $arguments)
    {
        //404访问不存在
        return $this->create([],'资源不存在~',404);
    }
}