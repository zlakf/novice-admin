<?php


namespace app\index\controller;


/**
 * Class Error
 * @package app\api\controller
 */
class Error extends Base
{
    //404
    public function index(){
        //404访问不存在
        return $this->create([],'资源不存在~',404);
    }
}