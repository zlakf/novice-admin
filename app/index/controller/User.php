<?php


namespace app\index\controller;


use think\facade\Db;
use app\index\model\User as userModel;
class User extends Base
{
    /**
     * 测试数据
     * @author 周蕾
     * @return \think\Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        //获取数据列表
        $data=userModel::field('*')->paginate([
            'list_rows'=>4,
            'var_page' => 'page',
        ]);
        //判断是否有值
        if($data->isEmpty()){
            return $this->create($data,'数据为空~',204);
        }else{
            return $this->create($data,'数据请求成功~',200);
        }

    }
    public function insert()
    {
        $data=[
            'pid'=>1,
            'sid'=>1,
            'phone'=>18208925160,
            'login_name'=>'zl737429860',
            'login_pass'=>'zhoulei'
        ];
        Db::name('user')->insert($data);
    }
}