<?php
/**
 *
 * @author: zhoulei
 * @time: 2020/10/20
 *
 */


namespace app\validate\admin;
use think\Validate;

class AdminUser extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>['规则1'，'规则2',...]
     * @var array
     */
    protected $rule = [
        'username' => 'require|max:20',
        'password' => 'require|max:20'
    ];
    /**
     * 定义错误信息
     * 格式：'字段名' =>['规则1'，'规则2',...]
     * @var array
     */
    protected $message  =   [
        'username.require' => '用户名不为空',
        'username.max' => '用户名不超过20个字符',
        'password.require' => '密码不为空',
        'password.max' => '密码不超过20个字符'
    ];
}