<?php /*a:1:{s:37:"E:\tp6\app\admin\view\time\index.html";i:1603165198;}*/ ?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>当前时间</title>
    <style>
        html,body{
            height: 100%;
            width: 100%;
            background: #0cc;
        }
    </style>
</head>
<body>
<div id="time" style="
    text-align: center;
    position: absolute;
    width: 80%;
    height: 80px;
    line-height: 80px;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    margin: auto;
    font-size: 96px;"></div>
<script type="text/javascript">
    function time(){
        var vWeek,vWeek_s,vDay;
        vWeek = ["星期天","星期一","星期二","星期三","星期四","星期五","星期六"];
        var date =  new Date();
        console.log(date);
        year = date.getFullYear();
        month = date.getMonth() + 1;
        day = date.getDate();
        hours = date.getHours();
        minutes = date.getMinutes();
        seconds = date.getSeconds();
        vWeek_s = date.getDay();
        if(minutes <10){
            minutes='0'+minutes;
        }
        if(seconds <10){
            seconds='0'+seconds;
        }
        document.getElementById("time").innerHTML = year + "年" + month + "月" + day + "日" + "\t" + hours + ":" + minutes +":" + seconds + "\t" + vWeek[vWeek_s] ;

    };
   var flag= setInterval("time()",1000);
   //回车清除定时器
    document.onkeydown = function(e){

        var ev = document.all ? window.event : e;

        if(ev.keyCode==13) {
            // 如（ev.ctrlKey && ev.keyCode==13）为ctrl+Center 触发
            clearInterval(flag);
            //要处理的事件

        }

    }
</script>
</body>
</html>
