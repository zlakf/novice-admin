<?php /*a:1:{s:41:"E:\tp6\app\admin\view\auth_rule\edit.html";i:1603937778;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form layuimini-form layui-col-md6">
    <input type="hidden" name="id" value="<?php echo htmlentities($data['id']); ?>">
    <div class="layui-form-item">
        <label class="layui-form-label required">父级</label>
        <div class="layui-input-block">
            <input type="text" readonly class="layui-input" lay-verify="required" id="p_title" value="<?php echo htmlentities($data['ptitle']); ?>">
            <input type="hidden" name="pid" id="pid" value="<?php echo htmlentities($data['pid']); ?>">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label required">标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="required" lay-reqtext="标题不能为空" placeholder="请输入标题" value="<?php echo htmlentities($data['title']); ?>" class="layui-input" autocomplete="off">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">规则</label>
        <div class="layui-input-block">
            <input type="text" name="name" lay-verify="required" lay-reqtext="规则不能为空" placeholder="请输入规则" value="<?php echo htmlentities($data['name']); ?>" class="layui-input" autocomplete="off">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">类型</label>
        <div class="layui-input-block">
            <input type="radio" name="type" value="file" title="权限节点" <?php if($data['type']=='file'): ?>checked<?php endif; ?>>
            <input type="radio" name="type" value="menu" title="菜单" <?php if($data['type']=='menu'): ?>checked<?php endif; ?>>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">开关</label>
        <div class="layui-input-block">
            <input type="checkbox" name="status" lay-skin="switch" lay-filter="switchTest" lay-text="开启|关闭" <?php if($data['status']==1): ?>checked<?php endif; ?>>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">权重</label>
        <div class="layui-input-block">
            <input type="number" name="weigh" placeholder="请输入权重" value="<?php echo htmlentities($data['weigh']); ?>" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认修改</button>
        </div>
    </div>
</div>
<div class="layui-form layuimini-form layui-col-md6">
    <div id="test1"></div>
</div>
<script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="/static/js/jquery.min.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.$;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            if(data.field.status == "on") {
                data.field.status = "1";
            } else {
                data.field.status = "0";
            }
            $.ajax({
                // url:"<?php echo url('auth_rule/addData'); ?>",
                url: "editData", //后台数据请求地址
                type:'post',
                dataType:'json',
                // data:JSON.stringify(data.field),
                data:data.field,
                success:function (result) {
                    layer.msg(result.msg);
                    setTimeout(function(){
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                    },1000);
                    console.log(result);
                },
                error:function(result) {
                    layer.msg(result.msg,{icon:1,time:1000});
                },
            });
        });
    });

    layui.use('tree', function(){
        var tree = layui.tree;
        //渲染
        var inst1 = tree.render({
            elem: '#test1',  //绑定元素
            data: getData(),
            click: function(obj){
                $("#pid").val(obj.data.id);
                $("#p_title").val(obj.data.title);
            }
        });
    });

    function getData(){
        var data = [];
        $.ajax({
            url: "treeRule", //后台数据请求地址
            type: "post",
            async:false,
            success: function(resut){
                data = resut['data'];
            }
        });
        return data;
    }
</script>
</body>
</html>