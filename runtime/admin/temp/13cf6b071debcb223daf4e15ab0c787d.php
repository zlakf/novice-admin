<?php /*a:1:{s:41:"E:\tp6\app\admin\view\auth_group\add.html";i:1604049294;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form layuimini-form layui-col-md12">
    <div class="layui-form-item">
        <label class="layui-form-label required">角色名称</label>
        <div class="layui-input-block">
            <input type="text" name="title" lay-verify="required" lay-reqtext="角色名称不能为空" placeholder="请输入角色名称" value="" class="layui-input" autocomplete="off">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">开关</label>
        <div class="layui-input-block">
            <input type="checkbox" checked="" name="status" lay-skin="switch" lay-filter="switchTest" lay-text="开启|关闭">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">选择权限</label>
        <div class="layui-form-label" id="test1"></div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" name="rules">
            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认添加</button>
        </div>
    </div>
</div>
<script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script src="/static/js/jquery.min.js" charset="utf-8"></script>
<script>
    layui.use(['form','tree'], function () {
        var form = layui.form,
            layer = layui.layer,
            tree = layui.tree,
            $ = layui.$;

        //渲染
        var inst1 = tree.render({
            elem: '#test1',  //绑定元素
            data: getData(),
            id: 'id', //定义索引
            showCheckbox: true,
        });

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            var checkData = tree.getChecked('id');
            var rules = new Array();
            rules = getChecked_list(checkData);
            data.field.rules = rules;

            if(data.field.status == "on") {
                data.field.status = "1";
            } else {
                data.field.status = "0";
            }
            $.ajax({
                url: "addData", //后台数据请求地址
                type:'post',
                dataType:'json',
                data:data.field,
                success:function (result) {
                    layer.msg(result.msg, {
                        icon: 1,
                        time: 1000
                    }, function(){
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                    });
                },
                error:function(result) {
                    layer.msg(result.msg,{icon:2,time:1000});
                },
            });
        });

        // 获取选中节点的id
        function getChecked_list(data) {
            var id = "";
            $.each(data, function (index, item) {
                if (id != "") {
                    id = id + "," + item.id;
                }
                else {
                    id = item.id;
                }
                var i = getChecked_list(item.children);
                if (i != "") {
                    id = id + "," + i;
                }
            });
            return id;
        }

        function getData(){
            var data = [];
            $.ajax({
                url:"<?php echo url('auth_rule/treeRule'); ?>",
                type: "post",
                async:false,
                success: function(resut){
                    data = resut['data'];
                }
            });
            return data;
        }

    });
</script>
</body>
</html>