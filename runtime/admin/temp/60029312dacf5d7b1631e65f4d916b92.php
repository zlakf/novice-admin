<?php /*a:1:{s:49:"E:\NoviceAdmin\app\admin\view\database\index.html";i:1605235136;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>在线命令</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/css/public.css" media="all">
</head>
<body>
<div class="layui-form layuimini-form layui-col-md12">
    <div class="layui-form-item">
      <div class="layui-inline">
        <label class="layui-form-label">选择数据表</label>
        <div class="layui-input-inline">
          <select name="tableName" lay-verify="required" lay-search="">
            <option value="">直接选择或搜索选择</option>
            <?php if(is_array($tables) || $tables instanceof \think\Collection || $tables instanceof \think\Paginator): $i = 0; $__LIST__ = $tables;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?>
            <option value="<?php echo htmlentities($list); ?>"><?php echo htmlentities($list); ?></option>
            <?php endforeach; endif; else: echo "" ;endif; ?>
          </select>
        </div>
      </div>
      <div class="layui-inline">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-block">
            <input type="text" name="icon" placeholder="请输入图标" value="" class="layui-input" autocomplete="off">
        </div>
      </div>
      <div class="layui-inline">
        <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认添加</button>
      </div>
    </div>
</div>


<script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script src="/static/js/jquery.min.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.$;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.ajax({
                url:"<?php echo url('Generate/codeGeneration'); ?>",
                //url: "addData", //后台数据请求地址
                type:'post',
                dataType:'json',
                // data:JSON.stringify(data.field),
                data:data.field,
                success:function (result) {
                    layer.msg(result.msg, {
                        icon: 1,
                        time: 1000
                    }, function(){
                        parent.location.reload();
                    });
                },
                error:function(result) {
                    layer.msg(result.msg,{icon:2,time:1000});
                },
            });
            // var index = layer.alert(JSON.stringify(data.field), {
            //     title: '最终的提交信息'
            // }, function () {
            //     // 关闭弹出层
            //     layer.close(index);
            //     var iframeIndex = parent.layer.getFrameIndex(window.name);
            //     parent.layer.close(iframeIndex);
            // });
            // return false;
        });

    });
</script>

</body>
</html>