<?php /*a:1:{s:38:"E:\tp6\app\admin\view\vruser\edit.html";i:1605164911;}*/ ?>
<!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8">
            <title>layui</title>
            <meta name="renderer" content="webkit">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
            <link rel="stylesheet" href="/static/css/public.css" media="all">
            <style>
                body {
                    background-color: #ffffff;
                }
            </style>
        </head>
        <body>
        <div class="layui-form layuimini-form">
                <div class="layui-form-item">
                        <label class="layui-form-label">推荐人(会员)</label>
                        <div class="layui-input-block">
                           <input type="email"  name=pid placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                        <label class="layui-form-label">顶级推荐人(店铺)</label>
                        <div class="layui-input-block">
                           <input type="email"  name=sid placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                        <label class="layui-form-label">电子邮件</label>
                        <div class="layui-input-block">
                           <input type="email"  name=email placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                        <label class="layui-form-label">手机号</label>
                        <div class="layui-input-block">
                           <input type="email"  name=phone placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                        <label class="layui-form-label">用户登录名</label>
                        <div class="layui-input-block">
                           <input type="email"  name=login_name placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                        <label class="layui-form-label">用户登录密码</label>
                        <div class="layui-input-block">
                           <input type="email"  name=login_pass placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                        <label class="layui-form-label">盐码</label>
                        <div class="layui-input-block">
                           <input type="email"  name=salt placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-inline">
                    <label class="layui-form-label">添加时间</label>
                    <div class="layui-input-inline">
                        <input type="text" name=add_time id=add_time lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
                    </div>
                </div><div class="layui-inline">
                    <label class="layui-form-label">更新时间</label>
                    <div class="layui-input-inline">
                        <input type="text" name=update_time id=update_time lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
                    </div>
                </div><div class="layui-form-item">
                        <label class="layui-form-label">备注信息</label>
                        <div class="layui-input-block">
                           <input type="email"  name=mark placeholder="请输入..." value="" class="layui-input">
                        </div>
                    </div>
                                <div class="layui-form-item">
                    <label class="layui-form-label">状态(0=禁用,1=启用)</label>
                    <div class="layui-input-block">
                        <input type="checkbox" checked="" name=status lay-skin="switch" lay-filter="switchTest" lay-text="开启|关闭">
                    </div>
                </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <input type="hidden" name="id" value="<?php echo htmlentities($data['id']); ?>">
                    <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认修改</button>
                </div>
            </div>
        </div>
        <script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
        <script>
            layui.use(['form','laydate'], function () {
                var form = layui.form,
                    layer = layui.layer,
                    $ = layui.$,
                    laydate = layui.laydate;
                    
                laydate.render({
                        elem: '#add_time'
                    });
                    
                laydate.render({
                        elem: '#update_time'
                    });
                    
        
                //监听提交
                form.on('submit(saveBtn)', function (data) {
                    if(data.field.status == "on") {
                data.field.status = "1";
            } else {
                data.field.status = "0";
            }
                    $.ajax({
                        url: "editData", //后台数据请求地址
                        type:'post',
                        dataType:'json',
                        data:data.field,
                        success:function (result) {
                            layer.msg(result.msg, {
                                icon: 1,
                                time: 1000
                            }, function(){
                                var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                                parent.layer.close(index); //再执行关闭
                            });
                        },
                        error:function(result) {
                            layer.msg(result.msg,{icon:2,time:1000});
                        },
                    });
                });
            });
        </script>
        </body>
        </html>