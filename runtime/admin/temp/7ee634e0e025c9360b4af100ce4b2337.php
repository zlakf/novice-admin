<?php /*a:1:{s:39:"E:\tp6\app\admin\view\vruser\index.html";i:1605164911;}*/ ?>
<!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <title>vruser</title>
            <meta name="renderer" content="webkit">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
            <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
            <link rel="stylesheet" href="/static/css/public.css" media="all">
        </head>
        <body>
        <div class="layuimini-container">
            <div class="layuimini-main">
        
                <fieldset class="table-search-fieldset">
                    <legend>搜索信息</legend>
                    <div style="margin: 10px 10px 10px 10px">
                        <form class="layui-form layui-form-pane" action="">
                            <div class="layui-form-item">
                               <div class="layui-inline">
                                    <label class="layui-form-label">用户ID</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=id autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">推荐人(会员)</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=pid autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">顶级推荐人(店铺)</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=sid autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">电子邮件</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=email autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">手机号</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=phone autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">用户登录名</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=login_name autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">用户登录密码</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=login_pass autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">盐码</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=salt autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">添加时间</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=add_time autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">更新时间</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=update_time autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">备注信息</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=mark autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">状态(0=禁用,1=启用)</label>
                                    <div class="layui-input-inline">
                                        <input type="text"  name=status autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                
                                <div class="layui-inline">
                                    <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </fieldset>
        
                <script type="text/html" id="toolbarDemo">
                    <div class="layui-btn-container">
                        <?php if($add=='true'): ?><button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button><?php endif; if($delete=='true'): ?><button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 删除 </button><?php endif; ?>
                    </div>
                </script>
        
                <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>
        
                <script type="text/html" id="currentTableBar">
                    <?php if($edit=='true'): ?><a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a><?php endif; if($delete=='true'): ?><a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a><?php endif; ?>
                </script>
        
            </div>
        </div>
        <script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
        <script>
            layui.use(['form', 'table'], function () {
                var $ = layui.jquery,
                    form = layui.form,
                    table = layui.table;
        
                table.render({
                    elem: '#currentTableId',
                    url: 'index',
                    parseData: function(res){ //res 即为原始返回的数据
                        // console.log(res);
                        return {
                            "code": res.code, //解析接口状态
                            "msg": res.msg, //解析提示文本
                            "count": res.data.total, //解析数据长度
                            "data": res.data.data //解析数据列表
                        };
                    },
                    toolbar: '#toolbarDemo',
                    defaultToolbar: ['filter', 'exports', 'print', {
                        title: '提示',
                        layEvent: 'LAYTABLE_TIPS',
                        icon: 'layui-icon-tips'
                    }],
                    cols: [[
                        {type: "checkbox", width: 50},
                        {field:'id', width: 120,title:'用户ID'},{field:'pid', width: 120,title:'推荐人(会员)'},{field:'sid', width: 120,title:'顶级推荐人(店铺)'},{field:'email', width: 120,title:'电子邮件'},{field:'phone', width: 120,title:'手机号'},{field:'login_name', width: 120,title:'用户登录名'},{field:'login_pass', width: 120,title:'用户登录密码'},{field:'salt', width: 120,title:'盐码'},{field:'add_time', width: 120,title:'添加时间'},{field:'update_time', width: 120,title:'更新时间'},{field:'mark', width: 120,title:'备注信息'},{field:'status', width: 120,title:'状态(0=禁用,1=启用)'},
                        {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
                    ]],
                    limits: [10, 15, 20, 25, 50, 100],
                    limit: 15,
                    page: true,
                    skin: 'line'
                });
        
                // 监听搜索操作
                form.on('submit(data-search-btn)', function (data) {
                    // JSON.stringify() 方法用于将 JavaScript 值转换为 JSON 字符串。
                    var result = JSON.stringify(data.field);
                    table.reload('currentTableId', {
                        page: {
                            curr: 1
                        }
                        , where: {
                            searchParams: result
                        }
                    }, 'data');
                    return false;
                });
        
                /**
                 * toolbar监听事件
                 */
                table.on('toolbar(currentTableFilter)', function (obj) {
                    if (obj.event === 'add') {  // 监听添加操作
                        var index = layer.open({
                            title: '添加管理员',
                            type: 2,
                            shade: 0.2,
                            maxmin:true,
                            shadeClose: true,
                            area: ['40%', '90%'],
                            content: 'add.html',
                            end: function(layero, index){
                                table.reload('currentTableId', {}, 'data');
                            }
                        });
                        $(window).on("resize", function () {
                            layer.full(index);
                        });
                    } else if (obj.event === 'delete') {  // 监听删除操作
                        var checkStatus = table.checkStatus('currentTableId')
                            , data = checkStatus.data;
                        console.log(JSON.stringify(data));
                        layer.confirm('真的要删除吗？', function (index) {
                            $.ajax({
                                url: "batchDelete", //后台数据请求地址
                                type:'post',
                                dataType:'json',
                                data:{'data':JSON.stringify(data)},
                                success:function (result) {
                                    layer.msg(result.msg, {
                                        icon: 1,
                                        time: 1000
                                    }, function(){
                                        table.reload('currentTableId', {}, 'data');
                                    });
                                },
                                error:function(result) {
                                    layer.msg(result.msg,{icon:2,time:1000});
                                },
                            });
                            layer.close(index);
                        });
                    }
                });
        
                //监听表格复选框选择
                table.on('checkbox(currentTableFilter)', function (obj) {
                    console.log(obj);
                });
        
                table.on('tool(currentTableFilter)', function(obj){
                    var data = obj.data;
                    if(obj.event === 'edit'){
                        layer.open({
                            type: 2
                            ,title: '编辑角色'
                            ,content: 'edit.html?id='+ data.id
                            ,maxmin: true
                            ,shadeClose : true
                            ,area: ['40%', '90%']
                            //编辑数据回显
                            ,end: function (layero, index) {
                                table.reload('currentTableId', {}, 'data');
                            }
                        });
                        $(window).on("resize", function () {
                            layer.full(index);
                        });
                    } else if (obj.event === 'delete') {
                        layer.confirm('真的要删除吗？', function (index) {
                            $.ajax({
                                url: "delete", //后台数据请求地址
                                type:'post',
                                dataType:'json',
                                data:{'id':data.id},
                                success:function (result) {
                                    layer.msg(result.msg, {
                                        icon: 1,
                                        time: 1000
                                    }, function(){
                                        table.reload('currentTableId', {}, 'data');
                                    });
                                },
                                error:function(result) {
                                    layer.msg(result.msg,{icon:2,time:1000});
                                },
                            });
                            layer.close(index);
                        });
                    }
                });
        
            });
        </script>
        
        </body>
        </html>
            