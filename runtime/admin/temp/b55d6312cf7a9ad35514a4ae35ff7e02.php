<?php /*a:1:{s:51:"E:\NoviceAdmin\app\admin\view\auth_group\index.html";i:1604481280;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/css/public.css" media="all">
</head>
<body>
<div class="layuimini-container">
    <div class="layuimini-main">

        <fieldset class="table-search-fieldset">
            <legend>搜索信息</legend>
            <div style="margin: 10px 10px 10px 10px">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">角色名称</label>
                            <div class="layui-input-inline">
                                <input type="text" name="title" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-inline">
                            <button type="submit" class="layui-btn layui-btn-primary"  lay-submit lay-filter="data-search-btn"><i class="layui-icon"></i> 搜 索</button>
                        </div>
                    </div>
                </form>
            </div>
        </fieldset>

        <script type="text/html" id="toolbarDemo">
            <div class="layui-btn-container">
                <?php if($add=='true'): ?><button class="layui-btn layui-btn-normal layui-btn-sm data-add-btn" lay-event="add"> 添加 </button><?php endif; if($delete=='true'): ?><button class="layui-btn layui-btn-sm layui-btn-danger data-delete-btn" lay-event="delete"> 删除 </button><?php endif; ?>
            </div>
        </script>

        <table class="layui-hide" id="currentTableId" lay-filter="currentTableFilter"></table>

        <script type="text/html" id="currentTableBar">
            <?php if($edit=='true'): ?><a class="layui-btn layui-btn-normal layui-btn-xs data-count-edit" lay-event="edit">编辑</a><?php endif; if($delete=='true'): ?><a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete" lay-event="delete">删除</a><?php endif; ?>
        </script>

    </div>
</div>
<script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'table'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table;

        table.render({
            elem: '#currentTableId',
            url: 'index',
            parseData: function(res){ //res 即为原始返回的数据
                // console.log(res);
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.data.total, //解析数据长度
                    "data": res.data.data //解析数据列表
                };
            },
            toolbar: '#toolbarDemo',
            defaultToolbar: ['filter', 'exports', 'print', {
                title: '提示',
                layEvent: 'LAYTABLE_TIPS',
                icon: 'layui-icon-tips'
            }],
            cols: [[
                {type: "checkbox", width: 50},
                {field: 'id', width: 80, title: 'ID'},
                // {field: 'pid', width: 120, title: '父级'},
                {field: 'title', width: 120, title: '角色名称'},
                // {field: 'rules', width: 150, title: '权限'},
                {field: 'createtime', width: 180, title: '创建时间'},
                {field: 'updatetime', width: 180, title: '更新时间'},
                {field: 'status', width: 100, title: '状态'},
                {title: '操作', minWidth: 150, toolbar: '#currentTableBar', align: "center"}
            ]],
            limits: [10, 15, 20, 25, 50, 100],
            limit: 15,
            page: true,
            skin: 'line'
        });

        // 监听搜索操作
        form.on('submit(data-search-btn)', function (data) {
            // JSON.stringify() 方法用于将 JavaScript 值转换为 JSON 字符串。
            var result = JSON.stringify(data.field);
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: {
                    searchParams: result
                }
            }, 'data');
            return false;
        });

        /**
         * toolbar监听事件
         */
        table.on('toolbar(currentTableFilter)', function (obj) {
            if (obj.event === 'add') {  // 监听添加操作
                var index = layer.open({
                    title: '添加角色',
                    type: 2,
                    shade: 0.2,
                    maxmin:true,
                    shadeClose: true,
                    area: ['40%', '100%'],
                    content: 'add.html',
                    end: function(layero, index){
                        table.reload('currentTableId', {}, 'data');
                    }
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {  // 监听删除操作
                var checkStatus = table.checkStatus('currentTableId')
                    , data = checkStatus.data;
                // layer.alert(JSON.stringify(data));
                layer.confirm('真的要删除吗？', function (index) {
                    $.ajax({
                        url: "batchDelete", //后台数据请求地址
                        type:'post',
                        dataType:'json',
                        data:{'data':JSON.stringify(data)},
                        success:function (result) {
                            layer.msg(result.msg, {
                                icon: 1,
                                time: 1000
                            }, function(){
                                table.reload('currentTableId', {}, 'data');
                            });
                        },
                        error:function(result) {
                            layer.msg(result.msg,{icon:2,time:1000});
                        },
                    });
                    layer.close(index);
                });
            }
        });

        //监听表格复选框选择
        table.on('checkbox(currentTableFilter)', function (obj) {
            console.log(obj);
        });

        table.on('tool(currentTableFilter)', function(obj){
            var data = obj.data;
            if(obj.event === 'edit'){
                layer.open({
                    type: 2
                    ,title: '编辑角色'
                    ,content: 'edit.html?id='+ data.id
                    ,maxmin: true
                    ,shadeClose : true
                    ,area: ['40%', '100%']
                    //编辑数据回显
                    ,end: function (layero, index) {
                        table.reload('currentTableId', {}, 'data');
                    }
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            } else if (obj.event === 'delete') {
                layer.confirm('真的要删除吗？', function (index) {
                    $.ajax({
                        url: "delete", //后台数据请求地址
                        type:'post',
                        dataType:'json',
                        data:{'id':data.id},
                        success:function (result) {
                            layer.msg(result.msg, {
                                icon: 1,
                                time: 1000
                            }, function(){
                                table.reload('currentTableId', {}, 'data');
                            });
                        },
                        error:function(result) {
                            layer.msg(result.msg,{icon:2,time:1000});
                        },
                    });
                    layer.close(index);
                });
            }
        });

    });
</script>

</body>
</html>