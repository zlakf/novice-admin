<?php /*a:1:{s:37:"E:\tp6\app\admin\view\admin\edit.html";i:1604110502;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/static/lib/layui-v2.5.5/css/layui.css" media="all">
    <link rel="stylesheet" href="/static/css/public.css" media="all">
    <style>
        body {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div class="layui-form layuimini-form">
    <div class="layui-form-item">
        <label class="layui-form-label required">用户名</label>
        <div class="layui-input-block">
            <input type="text" name="username" lay-verify="required" lay-reqtext="用户名不能为空" placeholder="请输入用户名" class="layui-input" value="<?php echo htmlentities($data['username']); ?>">
            <tip>填写自己管理账号的名称。</tip>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">修改密码</label>
        <div class="layui-input-block">
            <input type="password" name="password"  class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">昵称</label>
        <div class="layui-input-block">
            <input type="text" name="nickname" placeholder="请输入昵称" class="layui-input" value="<?php echo htmlentities($data['nickname']); ?>">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-block">
            <input type="email" name="email" placeholder="请输入邮箱" class="layui-input" value="<?php echo htmlentities($data['email']); ?>">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label required">角色</label>
        <div class="layui-input-block">
            <select name="group_id" lay-verify="required">
                <option value="">选择角色</option>
                <?php if(is_array($group) || $group instanceof \think\Collection || $group instanceof \think\Paginator): $i = 0; $__LIST__ = $group;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?>
                <option value="<?php echo htmlentities($list['id']); ?>" <?php if($list['id']==$data['group_id']): ?>selected="selected"<?php endif; ?>><?php echo htmlentities($list['title']); ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <input type="hidden" name="id" value="<?php echo htmlentities($data['id']); ?>">
            <button class="layui-btn layui-btn-normal" lay-submit lay-filter="saveBtn">确认修改</button>
        </div>
    </div>
</div>
<script src="/static/lib/layui-v2.5.5/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var form = layui.form,
            layer = layui.layer,
            $ = layui.$;

        //监听提交
        form.on('submit(saveBtn)', function (data) {
            $.ajax({
                url: "editData", //后台数据请求地址
                type:'post',
                dataType:'json',
                data:data.field,
                success:function (result) {
                    layer.msg(result.msg, {
                        icon: 1,
                        time: 1000
                    }, function(){
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index); //再执行关闭
                    });
                },
                error:function(result) {
                    layer.msg(result.msg,{icon:2,time:1000});
                },
            });
        });
    });
</script>
</body>
</html>